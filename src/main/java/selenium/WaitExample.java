package selenium;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class WaitExample extends BaseTest {
	
	@Test
	public void explicitWaitExample() throws InterruptedException {
		
		driver.get("https://the-internet.herokuapp.com/dynamic_loading/1");
		// incercam sa folosim cat mai putine atribute si dependinte
		//xpath: //button
		
		driver.findElement(By.xpath("//button")).click();
		WebElement finishText = driver.findElement(By.id("finish"));
		
		WebDriverWait wait = new WebDriverWait (driver, 10);
		wait.until(ExpectedConditions.textToBePresentInElement(finishText, "Hello World!"));
		
		assertEquals(finishText.getText(), "Hello World!");//redundant, conditie indeplinita mai sus, linia e doar pt readability
		
		
		
	}

}
