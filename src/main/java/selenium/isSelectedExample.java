package selenium;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class isSelectedExample extends BaseTest {

	
	public void isSelectedExample() {
		
		driver.findElement(By.className("menu_user_login")).click();
		
		WebElement username = driver.findElement(By.id("log"));
		WebElement rememberMeCheckBox = driver.findElement(By.id("rememberme"));
		
		username.isEnabled();//se aplica doar pt elemente de tip input care pot fi enabled/disabled--intoarce false;
		//pt restul elementele intoatrce true;
		
		rememberMeCheckBox.click();
		//se aplica doar pe elemente de tip input care au atribut type =  checkbox sau radiobutton
		//ex:<input type="checkbox" value="forever" id="rememberme" name="rememberme">
		assertTrue(rememberMeCheckBox.isSelected());//verifica daca acel element este selectat sau nu(radio button, checkbox)
		
	}
}
