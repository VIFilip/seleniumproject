package selenium;

import org.testng.annotations.Test;
@Test//adnotare la nivel de clasa
public class TestNgExample2 {
	
	//singura adnotare care are referinta catre o metoda psvm
	public static void one() {
		
		System.out.println("First");
	}
	
	public static void two() {
		
		System.out.println("Second");
	}
	
	public static void three() {
	
	System.out.println("Third");
}
	
	
	public static void four() {
	
	System.out.println("Fourth");
}
	
	 //ruleaza metodele care nu au prio setata, prima data
	public static void five() {
	
	System.out.println("Fifth");
}

}
