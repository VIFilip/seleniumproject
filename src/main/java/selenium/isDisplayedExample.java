package selenium;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class isDisplayedExample extends BaseTest {
	
	@Test
	public void exampleIsDisplayed() {
		
		WebElement username = driver.findElement(By.id("log"));
		WebElement password = driver.findElement(By.id("password"));
		
		driver.findElement(By.className("menu_user_login")).click();;
		assertTrue(username.isDisplayed());//verifica daca un anume element este in pagina
		assertTrue(password.isDisplayed());
		
	}

}
