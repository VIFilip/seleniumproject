package selenium;

import org.testng.annotations.Test;

public class TestNgExample {
	
	//@BeforeClass
	
	@Test(priority=0) //singura adnotare care are referinta catre o metoda psvm
	public static void one() {
		
		System.out.println("First");
	}
	@Test(priority=1)
	public static void two() {
		
		System.out.println("Second");
	}
	@Test(priority=2)
	public static void three() {
	
	System.out.println("Third");
}
	
	@Test
	public static void four() {
	
	System.out.println("Fourth");
}
	
	@Test //ruleaza metodele care nu au prio setata, prima data
	public static void five() {
	
	System.out.println("Fifth");
}

	
	
	

}
