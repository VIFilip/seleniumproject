package selenium;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.safari.SafariDriver.WindowType;
import org.testng.annotations.Test;

public class HomeworkAlerts extends BaseTest {

	
	
	@Test(priority=1)
	
	public void getBook() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"sc_tab_1456822345_1_17\"]/div/div[1]/h3/a")).click();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/the-forest/");
		Thread.sleep(3000);
		
	}
@Test(priority=2)
	
	public void clickOnReviews() throws InterruptedException {
	
	driver.findElement(By.xpath("//*[@id=\"tab-title-reviews\"]/a")).click();
	Thread.sleep(3000);
	
	driver.findElement(By.id("submit")).click();
		
	}

@Test(priority=3)

public void acceptAlert() throws InterruptedException {
	
	
	Thread.sleep(3000);
	Alert alertaJs = driver.switchTo().alert();
	alertaJs.dismiss();
	Thread.sleep(5000);
}

@Test(priority=4)

public void sendReview() throws InterruptedException {
	
	driver.findElement(By.xpath("//*[@id=\"commentform\"]/div[2]/p/span/a[5]")).click();
	Thread.sleep(3000);
	driver.findElement(By.name("comment")).sendKeys("Review OK");
	driver.findElement(By.name("author")).sendKeys("Filip");
	driver.findElement(By.name("email")).sendKeys("Filip@gmail.com");
	Thread.sleep(3000);
	driver.findElement(By.name("wp-comment-cookies-consent")).click();
	Thread.sleep(3000);
	driver.findElement(By.name("submit")).click();
	Thread.sleep(3000);
WebElement infoMessage = driver.findElement(By.className("woocommerce-review__awaiting-approval"));
Thread.sleep(5000);
	
assertTrue(infoMessage.getText().contains("Your review is awaiting approval"), "Your review is awaiting approval");
	
	
}
	
	
}
