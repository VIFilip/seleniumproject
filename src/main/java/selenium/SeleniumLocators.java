package selenium;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class SeleniumLocators extends BaseTest {

	//@Test
	public void idLocatorExample() {
		
		driver.findElement(By.id("menu_user")).click();
		
	}
	
	@Test(priority= 1)
	public void classNameLocatorExample() {
		
		driver.findElement(By.className("menu_user_login")).click();
		// --> //li[@class='menu_user_login']-->Xpath
		// --> li[class='menu_user_login']-->CSS selector
	}
	
	@Test(priority= 2)
	public void nameLocatorExample() {
		
		driver.findElement(By.name("pwd")).sendKeys("parola");
		driver.findElement(By.className("popup_close")).click();
		
	}
	
	@Test(priority= 3)
	public void tagNameLocatorExample() {
		
		WebElement discoverText = driver.findElement(By.tagName("em"));
		
		String actualText = discoverText.getText();
		System.out.println(actualText);
		assertEquals(actualText,"Discover");
	}
	
	@Test(priority= 4)
	
	public void linkTextLocatorExample() {
		
		driver.findElement(By.linkText("BOOKS")).click();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/");
	}
	
@Test(priority= 5)
	
	public void partiallinkTextLocatorExample() {
		
		driver.findElement(By.partialLinkText("forest")).click();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/the-forest/");
	}

@Test(priority= 6)

public void cssLocatorExample() {
	
	driver.findElement(By.cssSelector("button[name='add-to-cart']")).click();
	WebElement infoMessage = driver.findElement(By.cssSelector("div[class='woocommerce-message']"));
	
	assertTrue(infoMessage.getText().contains("�The forest� has been added to your cart."), "�The forest� has been added to your cart." );
}

@Test(priority= 7)

public void xpathLocatorExample() throws InterruptedException {
	// xpath by index --> expresia xpath intre () Ex: (//a[@class='button wc-forward'])[2] iar indexul este la final intre paranteze drepte
	Thread.sleep(3000);
	driver.findElement(By.xpath("(//a[@class='button wc-forward'])[2]")).click();
	
	
	assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/cart/" );
	driver.findElement(By.xpath("//span[@class='q_inc']")).click();
}
	
	
}
