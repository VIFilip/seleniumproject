package selenium.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import jdk.internal.org.jline.terminal.MouseEvent.Button;
import selenium.utils.BaseTest;

public class JSExecutorExample extends BaseTest {
	
	@Test
	public void jsExample1() throws InterruptedException {
		
		WebElement comingSoonLink = driver.findElement(By.linkText("COMING SOON"));
		//comingSoonLink.click();
		JavascriptExecutor js = (JavascriptExecutor)driver;
		
		js.executeScript("arguments[0].click()", comingSoonLink);
		Thread.sleep(5000);
		
		WebElement searchIcon = driver.findElement(By.cssSelector("button[class*='search_submit']"));
		js.executeScript("arguments[0].click()", searchIcon);
		
		WebElement searchField = driver.findElement(By.cssSelector(".search_field"));//trebuie punct inainte ca sa isi dea seama ca este css selector, altfel este numele clasei
		js.executeScript("arguments[0].value='cooking'", searchField);
		Thread.sleep(5000);
		
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ENTER).perform();
		Thread.sleep(5000);
		//locatori, metoda.page objects contin locatorii si metode specifice pe locatorii respectivi
		//testul meu face un ob al acelei pagini si cu o singura metoda poti sa asertezi pe rand toate cele 3 skills
		// fac o metodat care primeste locatorul ca aprametru si getText()
		//element.getText
	}

}
