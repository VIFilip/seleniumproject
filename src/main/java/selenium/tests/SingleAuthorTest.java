package selenium.tests;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import selenium.pages.LoginPage;
import selenium.pages.NavigationMenuPage;
import selenium.pages.ShopPage;
import selenium.pages.SingleAuthorPage;
import selenium.utils.BaseTest;

public class SingleAuthorTest extends BaseTest {
	

	
	@Test
	
	public void testSkills() throws InterruptedException {
		SingleAuthorPage singleAuthorPage = new SingleAuthorPage(driver);
		driver.get("https://keybooks.ro/team/margaret-robins/");
		Thread.sleep(8000);

		List<WebElement> skills = driver.findElements(By.xpath("//div[@class='sc_skills_total']"));
		
		System.out.println("Size: " + skills.size());
		
		System.out.println("Skill 1: " + skills.get(0).getText() );
		assertEquals(skills.get(0).getText(), "95%");
		System.out.println("Skill 2: " + skills.get(1).getText() );
		assertEquals(skills.get(1).getText(), "75%");
		System.out.println("Skill 3: " + skills.get(2).getText() );
		assertEquals(skills.get(2).getText(), "82%");
	
		
		
	}
	
	
	


}
