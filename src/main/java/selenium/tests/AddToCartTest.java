package selenium.tests;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import selenium.pages.CartPage;
import selenium.pages.NavigationMenuPage;
import selenium.pages.ShopPage;
import selenium.utils.BaseTest;

public class AddToCartTest extends BaseTest{

	
	@Test(priority = 1)
		public void filterByPrice() throws InterruptedException {
			NavigationMenuPage navPage = new NavigationMenuPage(driver);
			navPage.navigateTo(navPage.shopLink);
			ShopPage shopPage = new ShopPage(driver);			
			shopPage.dragAndDropSlider(shopPage.firstSlider, 72, 0);		
			shopPage.dragAndDropSlider(shopPage.lastSlider, -100, 0);
			
			shopPage.clickOnButton(shopPage.filterButton);			
			String actualText = driver.findElement(By.className("woocommerce-result-count")).getText();
			assertEquals(actualText, "Showing the single result");
	
			
			
			shopPage.clickOnButton(shopPage.addToCartButton);
			WebDriverWait wait = new WebDriverWait(driver, 10);
			
			
			shopPage.clickOnButton(shopPage.viewCartButton);
			
			
	}	
			
	@Test(priority = 2)
	
	public void editCart() throws InterruptedException {
		
		NavigationMenuPage menu = new NavigationMenuPage(driver);
		CartPage cartPage = new CartPage(driver);
		driver.navigate().to("https://keybooks.ro/cart/");
		cartPage.clickOnButton(cartPage.incProdNumButton);
		
		cartPage.clickOnButton(cartPage.updateCartButton);
		
		String actualText = driver.findElement(By.cssSelector("div[class=woocommerce-message]")).getText();
		assertEquals(actualText, "Cart updated.");
		
		
		List<WebElement> totalAmount = driver.findElements(By.xpath("//*[@class=\"woocommerce-Price-amount amount\"]"));
	
		assertEquals(totalAmount.get(3).getText(), "$29.98");
		
		
	}
	@Test(priority = 3)
public void addCouponCode() throws InterruptedException {
		CartPage cartPage = new CartPage(driver);
		driver.findElement(cartPage.couponField).sendKeys("WTM66DVT");
		
		driver.findElement(cartPage.applyCoupon).click();
		
		
		List<WebElement> totalAmount = driver.findElements(By.xpath("//*[@class=\"woocommerce-Price-amount amount\"]"));
		
		assertEquals(totalAmount.get(3).getText(), "$14.99");
	}

	
			
}



