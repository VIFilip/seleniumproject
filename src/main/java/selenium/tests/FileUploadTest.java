package selenium.tests;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import selenium.utils.BaseTest;

public class FileUploadTest extends BaseTest{
	
	@Test
	public void fileUploadTest() throws InterruptedException {
		
		driver.get("https://the-internet.herokuapp.com/upload");
		driver.findElement(By.id("file-upload")).sendKeys("C:\\Users\\Elena\\eclipse-workspace\\SeleniumProject\\fileTxt.txt");
		driver.findElement(By.id("file-submit")).click();
		//mai sus am facut sendkeys de path absolut si a functionat sa facem file upload cu selenium
		Thread.sleep(5000);
		
		String actualText = driver.findElement(By.id("uploaded-files")).getText();
		assertEquals(actualText, "fileTxt.txt1");
		
	}

}
