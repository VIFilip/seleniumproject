package selenium.tests;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import selenium.pages.BlogPage;
import selenium.pages.NavigationMenuPage;
import selenium.utils.BaseTest;

public class AudioTest extends BaseTest {
	
	@Test
	public void navigateToPostFormats() throws InterruptedException {
		
		NavigationMenuPage navPage = new NavigationMenuPage(driver);
		BlogPage blog = new BlogPage(driver);
		navPage.hoverMenu(navPage.blogLink);
		Thread.sleep(5000);;
		navPage.navigateTo(navPage.postFormatsLink);
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/category/post-formats/");		
		
		Thread.sleep(8000);;
		
		blog.clickOnButton(blog.audioPost);
		Thread.sleep(5000);;
		//asa am reusit sa fac sliderul activ, prin a da play 
		blog.clickOnButton(blog.playPauseButton);
		
		
		
		//blog.wait(10);
		//blog.playSong();
		//blog.pauseSong();
		Thread.sleep(8000);;
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		
		js.executeScript("document.getElementById('mep_0').setAttribute('style', 'transform: translateX(200px);')");
		Thread.sleep(8000);;
		//aici ar mai trebui sliderul pentru butonul de volum
		
		
	
	}

}
