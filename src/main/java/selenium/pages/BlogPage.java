package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BlogPage {
	
	public WebDriver driver;
	
	
	
	public BlogPage(WebDriver driver) {
		
		this.driver = driver;
	}
	
	public By audioPost = By.xpath("//a[text()='Audio post']");
	public By playPauseButton = By.xpath("//div[@class=\"mejs-button mejs-playpause-button mejs-play\"]");
	public By songSlider = By.cssSelector("span[class=\"mejs-time-handle\"]");
	public By volumeSlider = By.cssSelector("div[class=\"mejs-horizontal-volume-handle\"]");
	
	
	
	public void clickOnButton(By locator) {
		WebElement element = driver.findElement(locator);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();;
	}

	
	public void dragAndDropSlider(By locator, int x, int y) {
		WebElement element = driver.findElement(locator);
		Actions actions = new Actions(driver);
		actions.dragAndDropBy(element, x, y).perform();
	}
	
	public void doubleClick(By locator ) {
		WebElement element = driver.findElement(locator);
		Actions actions = new Actions(driver);
		actions.doubleClick(element).perform();
	
	}
	
	public void playSong() {
		WebElement playPauseButton = driver.findElement(By.xpath("//div[@class=\"mejs-button mejs-playpause-button mejs-play\"]"));
		
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		if (playPauseButton.getAttribute("title") == "Play" )
			{
		}else (playPauseButton).click();

}
	
	public void pauseSong() {
		WebElement playPauseButtons = driver.findElement(By.xpath("//div[@class=\"mejs-button mejs-playpause-button mejs-pause\"]"));
		playPauseButtons.click();
	}
}
