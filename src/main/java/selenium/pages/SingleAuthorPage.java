package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SingleAuthorPage {

	
	//instance variables
		public WebDriver driver;
		
		
	//constructor
		public SingleAuthorPage(WebDriver driver) {
			this.driver = driver;
		}
		
	//locator
		public By singleAuthorLink = By.linkText("SINGLE AUTHOR");
	
	//metode
		public void navigateTo(By locator) {
			driver.findElement(singleAuthorLink).click();
	
		}
		
		public String getElementText(By locator) {
			
			return driver.findElement(locator).getText();
		}
		
		
}

