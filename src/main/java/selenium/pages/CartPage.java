package selenium.pages;

import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CartPage extends ShopPage{
	
	

	public WebDriver driver;
	
	public CartPage(WebDriver driver) {
		super(driver);	}	
	
	
	public By incProdNumButton = By.cssSelector("span[class=q_inc]");
	public By decProdNumButton = By.xpath("//*[@class = \"q_dec\"]");
	public By updateCartButton = By.cssSelector("button[name=update_cart]");
	public By applyCouponButton = By.className("apply_coupon");
	public By checkoutButton = By.className("checkout-button button alt wc-forward");
	public By totalAmount = By.className("woocommerce-Price-amount amount");
	public By couponField = By.id("coupon_code");
	public By applyCoupon = By.xpath("//*[@name='apply_coupon']");
	
	
	


	
}
