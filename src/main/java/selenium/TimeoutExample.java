package selenium;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

public class TimeoutExample {
	
	@Test(timeOut = 3000)
	public void metod1() throws InterruptedException {
		
		Thread.sleep(4000);
		System.out.println("Method 1");
	}
	
	@Test
	public void metod2() {
		
		System.out.println("Method 2");
	}
	
	@Test
	public void metod3() {
		
		System.out.println("Method 3");
	}

}
