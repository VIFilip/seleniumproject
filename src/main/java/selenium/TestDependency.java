package selenium;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

public class TestDependency {
	
	@Test
	public void metod1() {
		assertTrue(false);
		System.out.println("Method 1");
	}
	
	@Test(dependsOnMethods = "metod1")
	public void metod2() {
		
		System.out.println("Method 2");
	}
	
	@Test(dependsOnMethods = "metod1")
	public void metod3() {
		
		System.out.println("Method 3");
	}

}
